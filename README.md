---

# Introduce #
* Project Name : ***드론대난투***  
* Responsibility Team : Aerospace SW Project Team 4. ***Tejava***  
* Deadline : ***17/06/12*** (예정-Final exam at 17/06/15)  

---

### 아래와 같은 것을 작성하는 곳입니다. ###
* 설정, 설치, 명령 정보 (가이드라인)  
* 종속성 정보  
* 테스트 방법  
* 코드 리뷰  
* etc..  

---

### 공지사항 ###
* 17.05.06 : [이슈][issue]에 [최종계획서 관련 작성 글][issue1] 참고  

[issue]: https://bitbucket.org/tejawa1/drondnt/issues/
[issue1]: https://bitbucket.org/tejawa1/drondnt/issues/1/--------
  
---
### git 사용 방법 ###
* [Git 사용법][git_guide] : 좌측 토픽에도 여러 목록이 있음. (로컬저장소, 원격저장소, merge, commit, branch, pull 에 대해서는 무조건 알아야 하고 기타 충돌에 대해서 등도 숙지해야 좋습니다.)
* [Git용 GUI프로그램][git_gui] : 아무거나 사용하면 됨.

[git_guide]: https://opentutorials.org/course/1492
[git_gui]: http://www.omnibuscode.com/board/board_versioncontrol/35743
---

### Scope (17.05.04) ###

* First - 최대한 빨리 :fast_forward:(진행중)
~~~
#!javascript
Resource
 1 : 드론
 2 : 안드로이드카메라 (1인칭카메라 없음)
 3 : 서버 (석창이서버)
 4 : 표적 그림 2장

Output
 1 : 안드로이드 앱 카메라로 인식되는 드론을 공격할 때 체력을 줄이고 두 개의 앱이 동기화 됨.
 2 : 안드로이드로 로터속도 제어 (타격시 로터 전체에 속도 장애)

real
 1 : 드론(표적그림달고있음), 사람(표적그림 배에 붙임)
 2 : 조종사 2명(앱 2개)이 서로의 표적을 겨누고 공격버튼 PUSH
 3 : 드론조종사는 실제 조종 (no prop)
~~~
>  
> 안드로이드 앱 (UI)  
> > 버튼 들은 작동하는가? (로터 속도 제어도 되는가?)  
  > 버튼에 상호작용하여 상대 플레이어와 값이 동기화 되는가?(체력)  
  >  

> 드론 인식 (CV)  
> > 기본적인 도형을 인식할 수 있는가?  
  >

> ROS통신  
> > 드론을 움직일 경우 변하는 state value를 받을 수 있는가?  
  > 두 android 앱이 동기화가 되는가?  
  > 드론의 로터가 안드로이드 앱으로 제어가능한가?  
  >  
>  

* Second - :triangular_flag_on_post:(목표)
~~~
#!javascript
Resource
 1 : 드론
 2 : <1인칭카메라 구입>
 3 : 서버 (석창이서버)
 4 : 표적 그림 2장

Output
 1 : 1인칭 카메라에서의 도형 인식
 2 : 타격시 로터속도 일부 장애발생
 3 : 음향키로 공격
 4 : ROS 안전장치

real
 1 : first와 같음. (기능적 면에서 잘 작동하는지 검토)
~~~
> Android UI  
> > 음향키를 눌렀을 때 공격이 잘 되는가?  
  > 그럼 소리는 어떻게 대체? (무음?) 에 대한 해결  
  > 기타 UI 접근성 향상  
  >  
> Android CV  
> > 화질이나 빛 간섭에 약한 1인칭 카메라로 도형인식을 무리없이 해낼 수 있는가?  
  >   
> ROS  
> > 타격을 받았을 때 일부 로터의 속도를 전처리하여 제어할 수 있는가?    
  > 특정 이하의 높이이거나 안드로이드 접속이 끊겼을 때 ROS에서 이를 감지하고 드론이 호버링하거나 정지하게 할 수 있는가?    
>  

* Final - :rocket:(로망, 가능하다면 구현)
~~~
#!javascript
Resource
 1 : 드론
 2 : 1인칭카메라
 3 : 서버 (석창이서버)

Output  
 1 : 1인칭 카메라에서의 드론 인식  
 2 : 드론사이의 거리 예측(MEMS, CV)  
 3 : 거리예측을 통한 실제 타격위치 포착(어느 방향 로터)  
 4 : 드론 자체 safety mode  
 5 : markerless ar 사용, 협력레이드(공룡사냥) / 多:多 전투
 6 : sound, effect  
 7 : 범용성  

real  
 1 : 실제로 드론을 띄우고 드론끼리 싸우거나,  
 2 : 드론그림의 표적을 갖고있는 인간과 싸움  
~~~
> Android UI  
> > 슈팅 effect의 화려함 정도.  
  > 입힌 sound가 잘 작동하는가?  
  >   
> Android CV  
> > 드론 자체를 인식할 수 있는가? ( 사실상 :x: )  
  > 자신과 상대 드론의 거리를 예측할 수 있는가? ( 사실상 :x: - CV 이거나 MEMS 중 오차가 적은 것으로)  
  > 예측한 거리를 이용하여 어느 로터에 가깝게 맞췄는지 계산하고 로터별로 데미지를 주게 할 수 있는가?  
  > 해당 카메라 뿐 아니라 여러 카메라에서 작동할 수 있는 이미지 인식 ( 사실상 :x: )  
  >   
> Pixhawk  
> > ROS와 Android 통신이 끊겼을때 이를 감지하고 호버링하거나 안정적으로 정지할 수 있는가?  
  >   
> Camera  
> > 온보드 카메라, wifi 카메라 모두가 지원 가능한가? (범용성)  
  >    
> Network  
> > 어떠한 pixhawk로도 {카메라, 앱, pixhawk 프로그램}만 있으면 해당 게임이 실행 가능한가? (범용성)  
>